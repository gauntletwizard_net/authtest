# docker build -t registry.gitlab.com/gauntletwizard_net/misc/authtest
FROM golang:buster as build

COPY ./go.mod ./go.sum ./main.go /go/src/gitlab.com/gauntletwizard_net/authtest/
WORKDIR /go/src/gitlab.com/gauntletwizard_net/authtest/

RUN go mod download

COPY ./ /go/src/gitlab.com/gauntletwizard_net/authtest
WORKDIR /go/src/gitlab.com/gauntletwizard_net/authtest


RUN go install ...
