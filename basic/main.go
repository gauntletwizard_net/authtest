package main

import (
	"net/http"
)

func main() {
	a := AuthServer{}
	http.HandleFunc("/auth", a.SetUsername)
	http.HandleFunc("/", a.Login)
	http.HandleFunc("/username", a.GetLogin)
	http.ListenAndServe(":8080", nil)
}

const usernameCookie = "Username"

type AuthServer struct{}

func (_ AuthServer) SetUsername(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username := r.Form.Get("username")
	http.SetCookie(w, &http.Cookie{
		Name:  usernameCookie,
		Value: username,
	})
	http.Redirect(w, r, "/username", http.StatusFound)
}

func (_ AuthServer) Login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	form := `
	<html><body>
		<form action="/auth">
			<input type="email" id="username" name="username">
			<button type="submit">Submit</button>
	</form></body></html>
`
	w.Write([]byte(form))
}

func (_ AuthServer) GetLogin(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	cookie, _ := r.Cookie(usernameCookie)
	var username string
	if cookie != nil {
		username = cookie.Value
	}

	w.Write([]byte(username))
}
