REV="$(git describe --match=NeVeRmAtCh --always --abbrev=40 --dirty)"
TAG="registry.gitlab.com/gauntletwizard_net/misc/authtest:${REV}"
docker build -t "${TAG}" .
docker push "${TAG}"
kubectl set image -f ./workos/deploy.yaml "*=${TAG}"
