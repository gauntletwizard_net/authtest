package main

import (
	"fmt"
	"net/http"

	"github.com/workos-inc/workos-go/pkg/sso"
)

// Test environment key for workos
const workos_apikey = "sk_a5UvGB4LQW0WeYMUGc0agwYKi"
const workos_clientid = "client_01F75ZARDG0S8CTQ3613GD7PZT"

const base_url = "https://authtest.do.gauntletwizard.net/"

func main() {

	sso.Configure(workos_apikey, workos_clientid)

	a := AuthServer{}
	http.HandleFunc("/", a.Login)
	http.HandleFunc("/auth", a.SetUsername)
	http.HandleFunc("/username", a.GetLogin)

	w := WorkOSAuthServer{}

	http.HandleFunc("/workos/", w.LoginWorkOS)
	http.HandleFunc("/workos/redirect", w.LoginRedirect)
	http.HandleFunc("/workos/callback", w.Callback)

	err := http.ListenAndServeTLS(":8443", "/var/tls/tls.crt", "/var/tls/tls.key", nil)
	if err != nil {
		fmt.Println(err)
	}
}

const usernameCookie = "Username"
