package main

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/workos-inc/workos-go/pkg/sso"
)

// WorkOSAuthServer implements the functions for a Workos SSO Provider
// https://workos.com/docs/reference/sso
type WorkOSAuthServer struct {
}

/*
Authorization Flow:
LoginWorkOs ->
*/

func (_ WorkOSAuthServer) LoginWorkOS(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	form := `
	<html><body>
		<form action="/workos/redirect">
			<input type="email" id="username" name="username">
			<button type="submit">Submit</button>
	</form></body></html>
`
	w.Write([]byte(form))
}

func (_ WorkOSAuthServer) LoginRedirect(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	email := r.FormValue("username")
	emailParts := strings.Split(email, "@")
	if len(emailParts) != 2 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Failed to validate e-mail", email)
		return
	}
	domain := emailParts[1]
	fmt.Printf("Redirecting user ", email, domain)

	url, err := sso.GetAuthorizationURL(
		sso.GetAuthorizationURLOptions{
			Domain:      domain,
			RedirectURI: base_url + "workos/callback",
			State:       "foo bar",
		})
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Error from WorkOS", err)
		return
	}
	headers := w.Header()
	headers["Location"] = []string{url.String()}
	w.WriteHeader(http.StatusTemporaryRedirect)
	fmt.Fprintf(w, "Redirecting you to login for %s (%s)", domain, url)
}

func (_ WorkOSAuthServer) Callback(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	profile, err := sso.GetProfileAndToken(
		context.Background(),
		sso.GetProfileAndTokenOptions{
			Code: r.URL.Query().Get("code"),
		},
	)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "Invalid Login %s", err)
		return
	}
	fmt.Fprintf(w, "Logged in as %s (%s)", profile.Profile.ID, profile.Profile.Email)

}
